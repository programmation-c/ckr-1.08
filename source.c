#include <stdio.h>

int main() {
    
    int spaces = 0;
    int tabs = 0;
    int endOfLine = 0;

    int c = getchar();
    while ( c != EOF ) {
        
        if ( c == ' ')
            ++spaces;
        if ( c == '\t' )
            ++tabs;
        if ( c == '\n' )
            ++endOfLine;

        c = getchar();
    }

    printf("nombre d\' espaces : %d\n", spaces );
    printf("nombre de tabulations : %d\n", tabs );
    printf("nombre de sauts de ligne : %d\n", endOfLine );

    return 0;
}